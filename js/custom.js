function cookie_policy () {
	if ($.cookie('cookie_agreement') != 'accepted') {
		$('#pop-cookie').addClass('block-cookie--state-active');
	}
}
function audio_play() {
	var pl = document.getElementById('player');
	if ($('#player').length > 0) {
		$('#player').parents('.block-music').addClass('block-music--state-active');
		pl.play();
		$.cookie('cookie_bgmusic', 'played');
	}
}
function audio_stop() {
	var pl = document.getElementById('player');
	if ($('#player').length > 0) {
		$('#player').parents('.block-music').removeClass('block-music--state-active');
		pl.pause();
		$.cookie('cookie_bgmusic', 'played');
	}
}
function audio_stop_autoplay () {
	if ($.cookie('cookie_bgmusic') == 'played') {
		$('#player').removeAttr('data-autoplay').parents('.block-music').removeClass('block-music--state-active');
	}
}

$(document).ready(function(){

	function show_top_button(){
		if ( $(window).scrollTop() >= $(window).height() ){
			$('.to-top-button').each(function(){
				$(this).fadeIn(300);
			})
		} else {
			$('.to-top-button').each(function(){
				$(this).fadeOut(200);
			})
		}
	}

// FullPage plugin
	$('.content--full-screen').each(function(){
		$(this).fullpage({
			css3: true,
			sectionSelector: '.screen',
			//scrollOverflow: true,
			responsiveWidth: 1024,
			anchors:['slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7'],
			fixedElements: '.header, .to-top-button',
			lazyLoading: true,
			//bigSectionsDestination: 'bottom',
			//lockAnchors: false,
			// fitToSection: false,
			// normalScrollElements: '#join-the-team',
			animateAnchor: false,
			onLeave: function(){
				$('.slider--type-gallery .slider__inner').slick('slickNext');
				audio_stop_autoplay();
			}
			// afterLoad: function(origin, destination){
			// 	if ( ($.cookie('cookie_bgmusic') != 'played') || ($('#player', destination).length > 0) ) {
			// 		audio_play();
			// 	}
			// }
		});
	});

// Audio
	$('.block-music__icon--play').on('click', function(){
		audio_play();
		//console.log('Played!');
	});
	$('.block-music__icon--stop').on('click', function(){
		audio_stop();
		//console.log('Paused!');
	});
	if ($(window).width() < 1024){
		audio_stop_autoplay();
		audio_stop();
	}

// Mobile menu
	var scroll = '';
	$('.menu__button').on('click', function(){
		$('.menu__mobile').addClass('menu__mobile--state-active');
		scroll = $(window).scrollTop();
		$('html, body').css({
			'overflow': 'hidden',
			'height': '100%'
		});
	});
	$('.menu__close').on('click', function(){
		$('html, body').css({
			'overflow': 'visible',
			'height': 'initial'
		});
		$(window).scrollTop(scroll);
		$('.menu__mobile').removeClass('menu__mobile--state-active');
	});

// Sliders
	$('.slider--type-support').each(function(){
		var obj = $(this);
		$(this).slick({
			mobileFirst: true,
			customPaging: function(){
				return ''
			},
			appendDots: obj.parents('.block__content'),
			zIndex: 1,
			slidesToShow: 1,
			adaptiveHeight: true,
			dots: true,
			arrows: false,
			slidesToScroll: 1,
			fade: true,
			asNavFor: $('.slider--type-gallery .slider__inner', obj.parents('.slider-set'))
		})
	}).on('mouseenter', function(){
		$('.slider--type-gallery .slider__inner', $(this).parents('.slider-set')).slick('slickPause');
	}).on('mouseleave', function(){
		$('.slider--type-gallery .slider__inner', $(this).parents('.slider-set')).slick('slickPlay');
	});
	$('.slider--type-gallery').each(function(){
		var obj = $(this);
		$('.slider__inner', obj).slick({
			prevArrow: '<div class="slider__button slider__button--type-square slider__button--prev"></div>',
			nextArrow: '<div class="slider__button slider__button--type-square slider__button--next"></div>',
			appendArrows: obj,
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 1,
			adaptiveHeight: true,
			dots: false,
			arrows: true,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			//pauseOnFocus: false,
			//pauseOnHover: false,
			asNavFor: $('.slider--type-support', obj.parents('.slider-set'))
		});
	});
	$('.slider--type-team').each(function(){
		var obj = $(this).parents('.block-team');
		$(this).slick({
			prevArrow: $('.block-team__button--prev', obj),
			nextArrow: $('.block-team__button--next', obj),
			customPaging: function(){
				return ''
			},
			dotsClass: 'team-dots',
			appendDots: obj,
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 1,
			adaptiveHeight: true,
			dots: true,
			arrows: true,
			slidesToScroll: 1,
			fade: true
		});
	});
	$('.slider--type-services-list').each(function(){
		$(this).slick({
			prevArrow: '<div class="slider__button slider__button--type-simple slider__button--prev"></div>',
			nextArrow: '<div class="slider__button slider__button--type-simple slider__button--next"></div>',
			mobileFirst: true,
			zIndex: 1,
			adaptiveHeight: true,
			dots: false,
			arrows: true,
			focusOnSelect: true,
			slidesToScroll: 1,
			slidesToShow: 1,
			slidesPerRow: 1,
			asNavFor: $('.slider--type-services-description', $(this).parents('.block-services')),
			infinite: false,
			// centerMode: true,
			// centerPadding: '78px',
			//rows: 2,
			responsive: [
				{
					breakpoint: 1399,
					settings: {
						slidesToShow: 3,
						rows: 2,
						asNavFor: null
					}
				},
				{
					breakpoint: 959,
					settings: {
						slidesToShow: 2,
						rows: 2,
						asNavFor: null
					}
				},
				{
					breakpoint: 399,
					settings: {
						slidesToShow: 1,
						rows: 1,
						asNavFor: $('.slider--type-services-description', $(this).parents('.block-services'))
					}
				}
			]
		});
	});
	$('.slider--type-services-description').each(function(){
		$(this).slick({
			mobileFirst: true,
			zIndex: 1,
			adaptiveHeight: true,
			accessibility: false,
			dots: false,
			arrows: false,
			slidesToScroll: 1,
			slidesToShow: 1,
			fade: true,
			asNavFor: $('.slider--type-services-list', $(this).parents('.block-services')),
			responsive: [
				{
					breakpoint: 1399,
					settings: {
						asNavFor: null
					}
				},
				{
					breakpoint: 959,
					settings: {
						asNavFor: null
					}
				},
				{
					breakpoint: 399,
					settings: {
						asNavFor: $('.slider--type-services-list', $(this).parents('.block-services'))
					}
				}
			]
		}).on('swipe', function(slick, direction){
			if ($(window).width() >= 960) {
				var obj = $(this).parents('.block-services'),
					slave = $('.slider--type-services-list', $(this).parents('.block-services')),
					currentSlide = $(this).slick('slickCurrentSlide'),
					slaveItemsPerSlide = $('.block-service-item', $('.slick-slide', slave).eq(0)).length,
					slaveCurrentSlide = Math.floor(currentSlide/slaveItemsPerSlide),
					slaveActiveItemNumInSlide = currentSlide - (slaveCurrentSlide*slaveItemsPerSlide);

				// console.log('currentSlide: ' + currentSlide);
				// console.log('slaveItemsPerSlide: ' + slaveItemsPerSlide);
				// console.log('slaveCurrentSlide: ' + slaveCurrentSlide);
				// console.log('slaveActiveItemNumInSlide: ' + slaveActiveItemNumInSlide);
				// console.log('');

				$('.slider--type-services-list', obj).slick('slickGoTo', slaveCurrentSlide);
				$('.block-service-item', obj).removeClass('block-service-item--state-active');
				$('.slick-slide', $('.slider--type-services-list', obj)).eq(slaveCurrentSlide).children('div').eq(slaveActiveItemNumInSlide).children('.block-service-item').addClass('block-service-item--state-active');
			}
		});
	});
	$('.block-services-list__slider').on('click', '.block-service-item', function() {
		if ($(window).width() >= 960) {
			var obj = $(this).parents('.block-services'),
				currentSlide = $(this).parents('.slick-slide').index(),
				itemsPerSlide = $('.block-service-item', $(this).parents('.slick-slide')).length,
				activeItemNumInSlide = $(this).parents('.slick-slide > div').index(),
				currentItemNum = currentSlide * itemsPerSlide + activeItemNumInSlide;
			if ($(this).hasClass('block-service-item--state-active')) {
				return false;
			} else {
				$('.block-service-item', $(this).parents('.block-services-list__slider')).removeClass('block-service-item--state-active');
				$(this).addClass('block-service-item--state-active');
			}
			$('.slider--type-services-description', obj).slick('slickGoTo', currentItemNum);

			// console.log('currentSlide: ' + currentSlide);
			// console.log('itemsPerSlide: ' + itemsPerSlide);
			// console.log('activeItemNumInSlide: ' + activeItemNumInSlide);
			// console.log('currentItemNum: ' + currentItemNum);
			// console.log('');
		}
	});

// Form
	$('.input--type-text, .input--type-textarea').on('focus', function(){
		$(this).parents('.form__field').addClass('form__field--state-active');
	});
	$('.input--type-text, .input--type-textarea').on('blur', function(){
		if ($(this).val() == '') {
			$(this).parents('.form__field').removeClass('form__field--state-active');
		}
	});

// Styled file
	$('.input--type-file input:file').each(function(){
		if ($(this).val()!='') {
			$('.file', $(this).parents('.input--type-file')).text($(this).val().replace(/^.*[\\\/]/, ''));
			$(this).parents('.form__field').addClass('form__field--state-active');
		}
	});
	$('.input--type-file input:file').on('change', function(){
		$('.file', $(this).parents('.input--type-file')).text($(this).val().replace(/^.*[\\\/]/, ''));
		if ($(this).val()!='') {
			$(this).parents('.form__field').addClass('form__field--state-active');
		} else {
			$(this).parents('.form__field').removeClass('form__field--state-active');
		}
	});

// Map
	$('.block-map__map').each(function(){
		var map;
		function initialize() {
			var stylez = [
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e9e9e9"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 29
						},
						{
							"weight": 0.2
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 18
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dedede"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"saturation": 36
						},
						{
							"color": "#333333"
						},
						{
							"lightness": 40
						}
					]
				},
				{
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f2f2f2"
						},
						{
							"lightness": 19
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "administrative",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#fefefe"
						},
						{
							"lightness": 17
						},
						{
							"weight": 1.2
						}
					]
				}
			];
			var mapOptions = {
				zoom: 15,
				disableDefaultUI: true,
				zoomControl: true,
				center: new google.maps.LatLng(55.910109, 38.023753),
				scrollwheel: false,
				mapTypeControlOptions: {
					mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
				}
			};

			map = new google.maps.Map(document.getElementById('map'), mapOptions);
			var mapType = new google.maps.StyledMapType(stylez, { name: 'Grayscale' });
			map.mapTypes.set('tehgrayz', mapType);
			map.setMapTypeId('tehgrayz');

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(55.906599, 38.028423),
				map: map,
				icon: 'images/map-marker.png'
			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	});

// View More
	$('.next-slide').on('click', function () {
		$.fn.fullpage.moveSectionDown();
	});

// Pop-up
	$('.fancybox').fancybox({
		infobar: false,
		btnTpl: {
			smallBtn: '<div data-fancybox-close class="pop-up__close"></div>'
		}
	});

// To top button
	show_top_button();
	$(window).on('scroll', function(){
		show_top_button();
	});
	// $(window).on('resize', function(){
	// 	show_top_button();
	// });
	$('.to-top-button').on('click', function(){
		$.fn.fullpage.moveTo(1);
	});

// Cookie agreement
	setTimeout(cookie_policy, 1000);
	$('.close-block-cookie', $('#pop-cookie')).on('click', function () {
		$(this).parents('.block-cookie').removeClass('block-cookie--state-active');
		$.cookie('cookie_agreement', 'accepted');
	});

// Styled checkboxes
	$('.input--type-checkbox').each(function(){
		$('.input__checkbox', this).css('left','-9999px');
		$('.input__checkbox', this).after('<div class="checkbox"></div>');
		if ($('.input__checkbox', this).prop('checked') == true) {
			$(this).addClass('checked');
		}
	});
	$('.input--type-checkbox').on('click', '.checkbox, .input__label', function(){
		$(this).parents('.input--type-checkbox').toggleClass('checked');
		if ($(this).parents('.input--type-checkbox').hasClass('checked')){
			$('.input__checkbox', $(this).parents('.input--type-checkbox')).attr('checked','checked').prop('checked', true).change();
		} else {
			$('.input__checkbox', $(this).parents('.input--type-checkbox')).removeAttr('checked').prop('checked', false).change();
		}
	});

// Styled radio
	$('.input--type-radio').each(function(){
		$('.input__radio', this).css('left','-9999px');
		$('.input__radio', this).after('<div class="radio"></div>');
		if ($('.input__radio', this).prop('checked') == true) {
			$(this).addClass('checked');
		}
	});
	$('.input--type-radio').on('click', '.radio, .input__label', function(){
		$('.input--type-radio', $(this).parents('.set-input')).removeClass('checked').children('.input__radio').removeAttr('checked').prop('checked', false).change();
		$(this).parents('.input--type-radio').addClass('checked');
		$('.input__radio', $(this).parents('.input--type-radio')).attr('checked', 'checked').prop('checked', true).change();
	});

});